package com.example.lesson1.controller;

import com.example.lesson1.entity.Student;
import com.example.lesson1.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/shendili")
public class StudentController {

    private final StudentRepository repository;

    @GetMapping("/all")
    public ResponseEntity<List<Student>> getAll() {
        return ResponseEntity.ok(repository.findAll());
    }

    @GetMapping
    public ResponseEntity<?> hello() {
        return ResponseEntity.ok(List.of("Shendili\nTaypipish\nTaylepe\nTaydosh"));
    }

}
